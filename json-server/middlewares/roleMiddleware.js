const roleMiddleware = (req, res, next) => {
  if (req.method === 'GET') {
    return next()
  }

  const userRoles = req.user.userRoles
  const isAdmin = userRoles.includes('admin')

  if (isAdmin) {
    return next()
  }

  return res.sendStatus(403);
}

 module.exports =  roleMiddleware;