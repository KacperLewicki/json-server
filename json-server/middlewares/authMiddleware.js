const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  if (!req.headers.authorization) {
    return res.sendStatus(401)
  }

  const accessToken = req.headers.authorization.split(' ')[1]

  if (!accessToken) {
    return res.sendStatus(401)
  }

  jwt.verify(accessToken, process.env.ACCESS_TOKEN, (err, payload) => {
    if (err) {
      return res.sendStatus(401)
    }

    req.user = payload
    next()
  })
}

 module.exports = authMiddleware;