const userProtectMiddleware = (req, res, next) => {
  if (req.path.includes('user')) {
    return res.sendStatus(403);
  }
  next()
}

module.exports = userProtectMiddleware;