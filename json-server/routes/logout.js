const deleteRefreshToken = require("../utils/deleteRefreshToken.js");

const logout = (req, res) => {
  const {refreshToken} = req.body
  try {
    deleteRefreshToken(refreshToken)
    res.sendStatus(204)
  }

  catch (err) {
    res.sendStatus(500)
  }
}

module.exports = logout;

