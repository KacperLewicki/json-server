const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

const refreshTokens = require('../refreshTokens.json');

dotenv.config()

const refreshToken = (req, res) => {
  const { refreshToken } = req.body

  if (!refreshToken) {
    return res.sendStatus(400)
  }

  if (!refreshTokens.includes(refreshToken)) {
    return res.sendStatus(403)
  }

  jwt.verify(refreshToken, process.env.REFRESH_TOKEN, (err, payload) => {
    if (err) {
      return res.sendStatus(403)
    }

    console.log(payload)

    const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN, { expiresIn: '1h' })

    return res.json({ accessToken })
  })
}

module.exports = refreshToken;