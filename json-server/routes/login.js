const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

const db = require('../db.json');
const addRefreshToken = require('../utils/addRefreshToken.js');

dotenv.config()

const login = (req, res) => {
  const { username, password } = req.body

  if (!username || !password) {
    return res.sendStatus(400)
  }

  const user = db.users.find(user => user.username === username && user.password === password)

  if (!user) {
    return res.sendStatus(401)
  }

  const userRoles = db.usersRoles.find(userRole => userRole.userId === user.id)

  const payload = {userId: user.id, userRoles: userRoles.roles}
  const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN, { expiresIn: '1h' })
  const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN)

  try {
    addRefreshToken(refreshToken)
    return res.json({ accessToken, refreshToken })
  } catch (err) {
    return res.sendStatus(500)
  }
}

module.exports = login;