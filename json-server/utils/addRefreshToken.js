const fs = require('fs');

const refreshTokensPath = 'refreshTokens.json';

const addRefreshToken = (newObject) => {
  fs.readFile(refreshTokensPath, 'utf8', (err, data) => {
    if (err) {
      throw `Error reading the ${refreshTokensPath}, ${err}`;
    }

    const refreshTokens = JSON.parse(data);
    refreshTokens.push(newObject);

    const updatedRefreshTokens = JSON.stringify(refreshTokens, null, 2);

    fs.writeFile(refreshTokensPath, updatedRefreshTokens, 'utf8', (err) => {
      if (err) {
        throw `Error writing the ${refreshTokensPath} ${err}`;
      }
    });
  });
}

module.exports = addRefreshToken;