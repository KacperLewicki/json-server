const fs = require('fs');

const refreshTokensPath = 'refreshTokens.json';

const deleteRefreshToken = (token) => {
  fs.readFile(refreshTokensPath, 'utf8', (err, data) => {
    if (err) {
      throw `Error reading the ${refreshTokensPath}, ${err}`;
    }

    const refreshTokens = JSON.parse(data);
    const filteredRefreshTokens = refreshTokens.filter((item) => item !== token);

    const updatedRefreshTokens = JSON.stringify(filteredRefreshTokens, null, 2);

    fs.writeFile(refreshTokensPath, updatedRefreshTokens, 'utf8', (err) => {
      if (err) {
        throw `Error writing the ${refreshTokensPath} ${err}`;
      }
    });
  });
}

module.exports = deleteRefreshToken;