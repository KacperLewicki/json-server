const jsonServer = require('json-server');
const cors = require('cors');

const userProtectMiddleware = require('./middlewares/userProtectMiddleware.js');
const authMiddleware = require('./middlewares/authMiddleware.js');
const roleMiddleware = require('./middlewares/roleMiddleware.js');
const login = require('./routes/login.js');
const refreshToken = require('./routes/refreshToken.js');
const logout = require('./routes/logout.js');

const server = jsonServer.create()
const jsonRouter = jsonServer.router('db.json')
const jsonMiddlewares = jsonServer.defaults()

server.use(cors())
server.use(jsonServer.bodyParser)

server.post('/login', login)
server.post('/logout', logout)
server.post('/refresh-token', refreshToken)

server.use(userProtectMiddleware)
// server.use(authMiddleware)
server.use(roleMiddleware)
server.use(jsonMiddlewares)

server.use(jsonRouter)

server.listen(3001, () => {
  console.log('JSON Server is running')
})
